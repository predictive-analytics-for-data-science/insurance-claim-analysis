# Insurance claim analysis




## Description

This report focuses on finding insights or customer segmentation from insurance claim dataset consisting of customer information like claim amount, customer income, gender. We use methods like descriptive statistics (ex. mean, median), ttest, ANOVA, visual representations to answer important questions based around the data. 